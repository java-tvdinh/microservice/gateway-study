package com.bitrust.apigateway.exception;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ApplicationException extends RuntimeException {
    private String code;
    private String message;

    public ApplicationException(String code) {
        this.code = code;
    }

    public ApplicationException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getMessageRaw() {
        return message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
