package com.bitrust.apigateway.exception;


import com.bitrust.apigateway.error.ResponseError;
import lombok.Getter;
import lombok.Setter;

import java.text.MessageFormat;

@Setter
@Getter
public class ResponseException extends RuntimeException {

    private ResponseError error;
    private Object[] params;

    public ResponseException(ResponseError error) {
        this(error.getMessage(), null, error);
    }

    public ResponseException(String message, Throwable cause, ResponseError error) {
        this(message, cause, error, null);
    }

    public ResponseException(
            String message, Throwable cause, ResponseError error, Object... params) {
        super(MessageFormat.format(message, params), cause);
        this.error = error;
        this.params = params == null ? new Object[0] : params;
    }

    public ResponseException(String message, ResponseError error) {
        this(message, null, error);
    }

    public ResponseException(String message, ResponseError error, Object... params) {
        this(message, null, error, params);
    }

}
