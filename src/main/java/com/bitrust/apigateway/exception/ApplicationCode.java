package com.bitrust.apigateway.exception;

public enum ApplicationCode {
    SUCCESS("S200","Thành công"),
    INVALID_BODY("S901", "Body không hợp lệ"),
    AUTHENTICATION_FAIL("S902", "Xác thực không thành công"),
    UNKNOWN_ERROR("S101", "Lỗi không xác định"),
    DUPLICATE_REQUEST("70009","Duplicate token");
    private String code;
    private String description;

    ApplicationCode(String code, String description){
        this.code = code;
        this.description = description;
    }

    public String getCode(){
        return this.code;
    }
    public String getDescription(){
        return this.description;
    }
}
