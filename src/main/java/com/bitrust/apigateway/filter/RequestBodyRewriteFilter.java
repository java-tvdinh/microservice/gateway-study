package com.bitrust.apigateway.filter;

import com.bitrust.apigateway.dto.request.BaseRequest;
import com.bitrust.apigateway.utils.Const;
import com.bitrust.apigateway.utils.EncryptUtils;
import com.bitrust.apigateway.utils.PublicKeyString;
import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import org.json.simple.parser.JSONParser;
import org.reactivestreams.Publisher;
import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Configuration
@Log4j2
public class RequestBodyRewriteFilter implements RewriteFunction<String, String> {
    private static final Gson gson = new Gson();
    private static final JSONParser jsonParser = new JSONParser();

    @Override
    public Publisher<String> apply(ServerWebExchange exchange, String body) {
        try {
            log.info(body);
            BaseRequest request = gson.fromJson(body, Const.BaseRequestType);
            // decryption
            String wsRequest = EncryptUtils.RSADecrypt(request.getWsRequest().toString(), PublicKeyString.bitrustKey);
            Object object = jsonParser.parse(wsRequest);
            request.setWsRequest(object);
            return Mono.just(gson.toJson(request, Const.BaseRequestType));
        } catch (Exception ex) {
            log.error(
                    "An error occured while transforming the request body in class RequestBodyRewrite. {}",
                    ex);

            // Throw custom exception here
            throw new RuntimeException(
                    "An error occured while transforming the request body in class RequestBodyRewrite.");
        }
    }
}
