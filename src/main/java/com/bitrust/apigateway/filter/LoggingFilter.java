package com.bitrust.apigateway.filter;

import com.bitrust.apigateway.utils.Const;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.filter.factory.rewrite.ModifyRequestBodyGatewayFilterFactory;
import org.springframework.cloud.gateway.filter.factory.rewrite.RewriteFunction;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Set;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.*;

@Slf4j
@Component
@RequiredArgsConstructor
public class LoggingFilter implements GlobalFilter {

    private final ModifyRequestBodyGatewayFilterFactory modifyRequestBodyFilter;

    private final RewriteFunction requestBodyRewrite;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        Set<URI> originalUris = exchange.getAttribute(GATEWAY_ORIGINAL_REQUEST_URL_ATTR);
        if (originalUris != null) {
            URI originalUri = originalUris.iterator().next();

            Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);

            URI routeUri = exchange.getAttribute(GATEWAY_REQUEST_URL_ATTR);
            if (route == null) {
                log.info("Incoming request " + originalUri.toString() + " is routed to uri:" + routeUri);
            } else {
                log.info("Incoming request " + originalUri.toString() + " is routed to id: " + route.getId()
                        + ", uri:" + routeUri);
            }
        }
        if (Const.METHOD.POST.toString().equalsIgnoreCase(exchange.getRequest().getMethod().toString())) {
            return modifyRequestBodyFilter
                    .apply(
                            new ModifyRequestBodyGatewayFilterFactory.Config()
                                    .setRewriteFunction(String.class, String.class, requestBodyRewrite))
                    .filter(exchange, chain);
        }
        else{
            return chain.filter(exchange);
        }
    }
}
