package com.bitrust.apigateway.utils;

public final class PrivateKeyString {
    public static final String bitrustPrivateKey = "-----BEGIN PRIVATE KEY-----\n" +
            "MIIJQQIBADANBgkqhkiG9w0BAQEFAASCCSswggknAgEAAoICAQCgfzgOldVfNJ4J\n" +
            "enj/HPTrWwIXPQn6iUUWQ3MbQVl6OonTBuM3TiTMx0/KMcMMSCGj/1k5GZo9lZpo\n" +
            "F6W2ArFj+PKgfcNS8HcN1ton4NzSQWL8eMz7CGBpfa5Va1ggyYmyIS0sKEY8Fj9I\n" +
            "iqijkNgJgedgovS3NaaDhvwnCVsH08rCgDkAWCiHDzEXXJcc/79TVtmLiDJWYp/d\n" +
            "XIJAfAiMcyTradaxLx9+J9kBGMoOaiz+I9gBFU3WEpjle+iewRuoNjzpvWoGmY+s\n" +
            "1Hi51pjHys4qcJeGunc61URE+nfeCcXP//61lwFj75Tm7sgqxYpwZYspQkA8wQqq\n" +
            "Ap7vJmVRZvQoqYv5s211Is1kUUsfhn67UFXQz4TmutmYLg/KF1ExmQ5s/a9L2Xnf\n" +
            "DVO1KptMLvUxNAS1nROOB7bKlQS6kKcorSLX5SysyKJtIussfD1uUdOqAc3ehcoZ\n" +
            "45w3faJ1uO5/g6XksVaC/COWKeX2sxgGz0TPF3RDISXzka3Gdu9z3Qgfzc/aIOdB\n" +
            "KOJBrJcfMQ4qTZd4FjKliOFO0P8qxr9mk6c0jmA3Hmglw/315DbiCyeqmYBrVsz3\n" +
            "fP6+07sv5SJTy1Tl79HFFqLySbaScY3jH360UJGyAXHo19RmTQAI8aPm6VLk8u05\n" +
            "hsIHElixecuIBylR502VVEVKZT/KtQIDAQABAoICAEhAXTz7qL2eU1LTBqtaHsvh\n" +
            "AXTyro5Rw389SBFJ/vzuY2NTo3AfRqPFSh8s/bCGvFY2urTg2LXGpmcb1ia02aS2\n" +
            "yosDcq3gwMfDJBx1txMUCnq76xNLdxOmV5fuphLEh69gDhTl4JBmN4Dxou9XYloN\n" +
            "V9sCIQZlKZvmZZcxOACzEIE09fQ4u63pYeExdnnAjCH5ZB/TZU3MQkmFvsFG1RZd\n" +
            "M46i69qB6zyAtckvnq6Uj8Ks4i3CqHKplpkgid0F8+xq1ksnURa9hJd6YwrOF6NB\n" +
            "okQqwZwOT2R147DlzJZxcdKsXL7kxuR1U2QOtJ/EpAjrPZWKVJ7FDIbyfRgOAefH\n" +
            "33NYVkK8rcnfr7x17vdFpxXhAyykQF9kyIRGVNvcUVToN6dp6mdPgko+lTblpH7C\n" +
            "nT0GFcDjKpEThfh3YX1L+/C9AKvahMU/qKKpsucdlkdOcd2RvDlxmhtjtLMv9QgW\n" +
            "CosuJX2u2mUA1eOuNfyHq5KGmQJ1V958Rwi2jnwbNmIdqf+PUkj/ga5pdt3n4NgD\n" +
            "0PisoyucDUrzoqHfRAG9h5QUrGd+ZdUdVW5EZvT8V4YI663S9UrhXTcy3xM7c8vl\n" +
            "TLZdNxlwo9hYQq/Flds8o3myTDl871Cn5ffw8TLDCm8V5T2p9RqzUGFaZBBsqdJR\n" +
            "F5WuGdtfPZMkKCAsn9Y1AoIBAQDLBiWLn/V254AH5MTDB/jF7+BwQqJdbTmOe0ch\n" +
            "O53ENhZ9S1ZiZzve9Gj2L+FTnDYhqkE7ShG/E1nUagf59LjNsuneBG3q/iklZkeo\n" +
            "rTtEK0DTAO4t78d9yVrBaqk+6jI7GAq8IWM4qYvNcEfKseSrGG+ED8c+14KBw07R\n" +
            "BsLpg2R9Yp4VoF/3bu8sn0LIf63G4BvxHUTgVj//BoGk+MoiajNpEqh1i2LXEmR0\n" +
            "UQlSScX8RhpZdD6hBkWy2kdCRuwKQN7sil1FIEyidtvIvMTy+CxPMyrDqg60jR68\n" +
            "sungMI7eHl0oO/0sLQFSxmTZEffJv9w5F5hQiI3IFl+pyulDAoIBAQDKYEvQubBW\n" +
            "lazvBfw4naSU9hTl6gU1XsDnVZZChXzYsBtYvV+RNYALPKL8mfhzHqrPlwnesOnQ\n" +
            "WQmsWEHlPkjZSW5Bfx8OxFoj3XNZ94v0BZsFG7U14IJiuckWeo6PcxBC7Gy0JP+i\n" +
            "GXBj/vl5dFgGCVejIJzsjt7eoMn2o7X5JwxhLm3dEH0SFFFMc/CcV8sPh9D3MOuA\n" +
            "RIwyDg12HGQmyAS3ORA1Eot8MyDbJiO8gA+5YS33z1Ci2/l4YnSLQitS9SOZ8XHk\n" +
            "RZsXgC1fzniM2jm31cYWy0jkWMcGwXw/2lA9vdJNN7vXNejjSE5WB62F7Ymetrcx\n" +
            "dxR2H7HHLeCnAoIBAFsEnW50PvKYpJBlYkThXQk4GlGZvfrOjeAvqlrexEMD0J9J\n" +
            "WCfziibRQgEkxArc3HPJRUdUVJJqFXqiMoM/0F3/mNM8/x475axrpzKavxg3n+84\n" +
            "2o7jkXb1/2b473lSO0S86YsgGMB9Wjy22qinihGeedglzriAm3SbX8lcE6PmkPWZ\n" +
            "ca/WX0+pfzZtC96hRwIC1OQKsCc246fsfdC87LnzDKtNfxJ7c8th5oEzF0+53TXV\n" +
            "I9j4wV8lVbfyHcI703RhUs+O1HO6IEExsiTTZCRdWFgFY2Wm5sLRBXepaipk+pff\n" +
            "2vm/syfL4Wz/npSmdoUPK6a4ykNIhO+wACE548sCggEANWgKOe/28DKZq45pzEsV\n" +
            "1tXGGBKVA0uZHrOAbtepZcm7vcRkBv8WZmC/Jmg/n6m1LAlQ6nrHiVsl6Vx1L1kr\n" +
            "gmQVe6VffdclbpePTGu+fExKZ6OzQ5+W6RTcU1lOa97SqMaZvUSSTaXm+gycq4ef\n" +
            "fyBSB1Og9fCSMjA+fB8h87lxU+gbyB4E05OXDNT/51Jn3RZbeBBp5bSxRrhcpxHB\n" +
            "AFcCn5Ooikj1lHYtsoezIJivDZ3uspn8Tq48fJ+G4AU7R643kfWct8ACuMXzs3fa\n" +
            "allgp/n7R6Kn/tLK2yaOco9bY5/HcudFY/rvFaPMQHnQOirAiajwWVZyiOW+VbDu\n" +
            "uwKCAQBnbTdzipM2e8wggYAIia1Z3PoQrHm31gg/5HSPhjHLcBbmkIYkpi7cLmQm\n" +
            "0Z1/T7W0KMGEwpxacatT2JtpNR3bNQd6Mu9Hef6kB4+2C51rbTU+IHaW1tZwZR+3\n" +
            "ZPaLAFBYILgbBfs6t4Op7Nywb7St6+E/zog5Ep+DOQsN2WYtfsxssZD3pFG0pnDQ\n" +
            "5F59YIHRcEBFjJ+ORKp1JXo+xS9Odg6eShkV5lRgfhN50OQRSvqJ8KQ+yrPflkWR\n" +
            "0hebJEF7FLpFXSspQHgMdMrEV76nzdQk9GaYExk+y9HCdNYTe3vWFqWzkGkiSfMR\n" +
            "I6OjWeuj6At6m28KPtjzrEJnoA85\n" +
            "-----END PRIVATE KEY-----";
}
