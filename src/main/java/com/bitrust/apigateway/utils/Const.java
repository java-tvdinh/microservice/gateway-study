package com.bitrust.apigateway.utils;

import com.bitrust.apigateway.dto.request.BaseRequest;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Const {
    public static final String ALGORITHM_RSA_MODE = "RSA";

    public enum METHOD {
        GET,
        POST,
        PUT,
        PATCH
    }

    public static final Type BaseRequestType = new TypeToken<BaseRequest>() {}.getType();
}
