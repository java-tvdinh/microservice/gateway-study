package com.bitrust.apigateway.utils;

public class PublicKeyString {
    public static final String bitrustKey = "-----BEGIN PUBLIC KEY-----\n" +
            "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAoH84DpXVXzSeCXp4/xz0\n" +
            "61sCFz0J+olFFkNzG0FZejqJ0wbjN04kzMdPyjHDDEgho/9ZORmaPZWaaBeltgKx\n" +
            "Y/jyoH3DUvB3DdbaJ+Dc0kFi/HjM+whgaX2uVWtYIMmJsiEtLChGPBY/SIqoo5DY\n" +
            "CYHnYKL0tzWmg4b8JwlbB9PKwoA5AFgohw8xF1yXHP+/U1bZi4gyVmKf3VyCQHwI\n" +
            "jHMk62nWsS8ffifZARjKDmos/iPYARVN1hKY5XvonsEbqDY86b1qBpmPrNR4udaY\n" +
            "x8rOKnCXhrp3OtVERPp33gnFz//+tZcBY++U5u7IKsWKcGWLKUJAPMEKqgKe7yZl\n" +
            "UWb0KKmL+bNtdSLNZFFLH4Z+u1BV0M+E5rrZmC4PyhdRMZkObP2vS9l53w1TtSqb\n" +
            "TC71MTQEtZ0Tjge2ypUEupCnKK0i1+UsrMiibSLrLHw9blHTqgHN3oXKGeOcN32i\n" +
            "dbjuf4Ol5LFWgvwjlinl9rMYBs9Ezxd0QyEl85Gtxnbvc90IH83P2iDnQSjiQayX\n" +
            "HzEOKk2XeBYypYjhTtD/Ksa/ZpOnNI5gNx5oJcP99eQ24gsnqpmAa1bM93z+vtO7\n" +
            "L+UiU8tU5e/RxRai8km2knGN4x9+tFCRsgFx6NfUZk0ACPGj5ulS5PLtOYbCBxJY\n" +
            "sXnLiAcpUedNlVRFSmU/yrUCAwEAAQ==\n" +
            "-----END PUBLIC KEY-----";
}
