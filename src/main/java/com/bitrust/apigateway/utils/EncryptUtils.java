package com.bitrust.apigateway.utils;

import com.bitrust.apigateway.exception.ApplicationCode;
import com.bitrust.apigateway.exception.ApplicationException;
import lombok.extern.log4j.Log4j2;

import javax.crypto.Cipher;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Log4j2
public class EncryptUtils {

    public static String RSAEncrypt(String message, String privateKey) {
        // Tạo public key
        try {
            RSAPrivateKey rsaPrivateKey = getPrivateKeyFromString(privateKey);
            // Mã hoá dữ liệu
            Cipher c = Cipher.getInstance(Const.ALGORITHM_RSA_MODE);
            c.init(Cipher.ENCRYPT_MODE, rsaPrivateKey);
            byte encryptOut[] = c.doFinal(message.getBytes());
            String strEncrypt = Base64.getEncoder().encodeToString(encryptOut);
            return strEncrypt;
        } catch (Exception e) {
            log.error("RSAEncrypt fail");
            throw new ApplicationException(ApplicationCode.UNKNOWN_ERROR.getCode());
        }
    }

    public static String RSADecrypt(String message, String publicKey) {
        try {
            RSAPublicKey rsaPublicKey = getPublicKeyFromString(publicKey);
            Cipher c = Cipher.getInstance(Const.ALGORITHM_RSA_MODE);
            c.init(Cipher.DECRYPT_MODE, rsaPublicKey);
            byte decryptOut[] = c.doFinal(Base64.getDecoder().decode(
                    message));
            return new String(decryptOut);
        } catch (Exception exception) {
            log.error("RSADecrypt fail");
            throw new ApplicationException(ApplicationCode.UNKNOWN_ERROR.getCode());
        }
    }

    public static RSAPrivateKey getPrivateKeyFromString(String privateKey) throws IOException, GeneralSecurityException {
        String privateKeyPEM = privateKey;

        privateKeyPEM = privateKeyPEM.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
        byte byteData[] = Base64.getDecoder().decode(privateKeyPEM);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(byteData);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPrivateKey privKey = (RSAPrivateKey) kf.generatePrivate(keySpec);
        return privKey;
    }

    public static RSAPublicKey getPublicKeyFromString(String publicKey) throws IOException, GeneralSecurityException {
        String publicKeyPEM = publicKey;

        publicKeyPEM = publicKeyPEM.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
        byte byteData[] = Base64.getDecoder().decode(publicKeyPEM);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(byteData);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        RSAPublicKey pubKey = (RSAPublicKey) kf.generatePublic(keySpec);
        return pubKey;
    }

}
