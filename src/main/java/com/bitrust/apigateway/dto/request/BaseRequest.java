package com.bitrust.apigateway.dto.request;

import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.validation.annotation.Validated;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Validated
public class BaseRequest {
    String username;
    String token;
    @Valid Object wsRequest;
}
