package com.bitrust.apigateway.dto.response;

import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BaseResponse <T>{
    String errorCode;
    String message;
    @Valid T wsResponse;
}
