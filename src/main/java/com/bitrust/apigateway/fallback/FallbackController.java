package com.bitrust.apigateway.fallback;

import com.bitrust.apigateway.dto.response.Response;
import com.bitrust.apigateway.error.InternalServerError;
import com.bitrust.apigateway.exception.ResponseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class FallbackController {

    @RequestMapping("/fallback")
    public Response<Void> fallback() {
        throw new ResponseException(InternalServerError.SERVICE_UNAVAILABLE_ERROR);
    }

    @RequestMapping(value = "/fallback-1")
    @ResponseStatus
    public Mono<Map<String, Object>> fallback(ServerWebExchange exchange, Throwable throwable) {
        Map<String, Object> result = new HashMap<>(8);
        ServerHttpRequest request = exchange.getRequest();
        result.put("path", request.getPath().pathWithinApplication().value());
        result.put("method", request.getMethod().name());
        if (null != throwable.getCause()) {
            result.put("message", throwable.getCause().getMessage());
        } else {
            result.put("message", throwable.getMessage());
        }
        return Mono.just(result);
    }

}
