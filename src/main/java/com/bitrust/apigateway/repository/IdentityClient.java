package com.bitrust.apigateway.repository;

import com.bitrust.apigateway.dto.request.IntrospectRequest;
import com.bitrust.apigateway.dto.response.IntrospectResponse;
import com.bitrust.apigateway.dto.response.Response;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.service.annotation.PostExchange;
import reactor.core.publisher.Mono;

public interface IdentityClient {
    @PostExchange(url = "/auth/introspect", contentType = MediaType.APPLICATION_JSON_VALUE)
    Mono<Response<IntrospectResponse>> introspect(@RequestBody IntrospectRequest request);
}
