package com.bitrust.apigateway.service.impl;

import com.bitrust.apigateway.dto.response.IntrospectResponse;
import com.bitrust.apigateway.dto.response.Response;
import com.bitrust.apigateway.service.IdentityService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class IdentityServiceImpl implements IdentityService {

    public Mono<Response<IntrospectResponse>> introspect(String token) {
        return null;
    }

}
