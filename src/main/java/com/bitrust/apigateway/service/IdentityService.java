package com.bitrust.apigateway.service;

import com.bitrust.apigateway.dto.response.IntrospectResponse;
import com.bitrust.apigateway.dto.response.Response;
import reactor.core.publisher.Mono;

public interface IdentityService {
    Mono<Response<IntrospectResponse>> introspect(String token);
}
