package com.bitrust.apigateway.encryption;

import com.bitrust.apigateway.dto.request.BaseRequest;
import com.bitrust.apigateway.dto.request.EncryptionTestDto;
import com.bitrust.apigateway.utils.EncryptUtils;
import com.bitrust.apigateway.utils.PrivateKeyString;
import com.bitrust.apigateway.utils.PublicKeyString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.log4j.Log4j2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Type;
import java.util.Map;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-uat.properties")
@Log4j2
public class Encryption {
    @Value("${application.body.password}")
    private String password;

    @Test
    public void encryptBody() {
        String request = "{ \"description\": \"DATPT_TECH_5\", \"promotionCode\": \"DATPT_TECH_5\", \"applyOnIsdn\": true, \"promotionName\": \"DATPT_TECH_5\" }";
        //        BaseRequest<EncryptionTestDto> baseRequest = new BaseRequest<>();
//        Type stringType = new TypeToken<BaseRequest<String> >() {}.getType();
//        Gson gson = new Gson();
//        Map<String, Object> map = gson.fromJson(request, stringType);
        request = EncryptUtils.RSAEncrypt(request, PrivateKeyString.bitrustPrivateKey);
        log.info("Request: " + request);
    }

    @Test
    public void decryptBody() {
        String request = "TsP1QCdUv9IOBLxKr3VxLXcso0sgX7NuX1nwSTR1zVROuYdB6HK1Q1U31mx0N7vLB1T/9hVhnsNh0cGTOuqGY4sRVYG7vr1BWd2xYseOgmr8TOnIGTx/MT/c6HicCuVQPMNvqcYTi9z2T55kAyWmoM/I1pNOBT0Zg0+hI9VFTPwRxaCgJdevKc3LIJ2540DOBtzOsNoyO6kKCrpjZdEOnZ6wesugk8uCmMXHPppofZAAOcWEhY+0mxurs109gcz9x0UJOzcqfygyxj7FeI7pXFwZWLD4VzdU+0cUlaj2UY+/KeHDZvN8TER5JTrgK2DxCqRVALSu+RHm75cpqEvzGfEkyiqZ+4WbyH4cWQ7iKmr0Y3g+JPpSmEmtVIkUsTiwVMvbTdF8Jjx39X7O+bzO/oZ5ppPINMhCb2f6Wk85q7bK3q6uLZHMLJbAErPO15pwE3XX7EvNrRCIt+8kEL2s9yjhfnGd1E7SwTHZGnMzw4QaU3GXHxcBKLlDD4nxk9ukui68EGQR4lls6ml0op24kqgunK6IQ9KDzv3aRGrSbfMjKi+AWbvj2VsMajLOpGAqUA0gJZdAxj7JEd4HLmHQ6Zmq7zbwyvN7TiuUrmKBwFJFFb1b/prN36pnsKRYmW+nua+d3KZpJmYwh67KTerCVaAe60HPYRLKFeVfyCz2IdY=";
        request = EncryptUtils.RSADecrypt(request, PublicKeyString.bitrustKey);
        log.info("Request: " + request);
        Type stringType = new TypeToken<EncryptionTestDto>() {
        }.getType();
        Gson gson = new Gson();
        EncryptionTestDto requestBody = gson.fromJson(request, stringType);
        log.info(requestBody);
    }

    @Test
    public void decryptBody2() {
        String request = "{\n" +
                "    \"sessionId\": \"1b13e54c-362b-4de9-9879-bbbef4b995ca\",\n" +
                "    \"apiKey\": \"crm_createPromotionRecharge\",\n" +
                "    \"wsCode\": \"crm_createPromotionRecharge\",\n" +
                "    \"wsRequest\": \"DcgTHLqNWZjQQ6d4KCavRA0VmI+pGlpCryf8Ksga5MAwC1hH7awzc2XN9Xm7ecGDqKnSGdTiAS4Z6xOW5AwMsuWkkrMzyGnENZEOe5+vcbTch3aXX+AHhpvqFhomEbVgpCPrU10nxezKAD4/EJUOIUcbPytRdQodJ0pW+fmFWi/pltJzLzFtgbQSh3PhZB4kArXS2tsYRxjxmYjRaeDMPxxiF1buxZgvyO4w35rGThkQFNZLseeEKJIkvWeG9WM1thVZ5UcWsA1aHyGxaXRcTRbMAdY4B3oSrE8tfNh2CRL8itdQyu3eGqTO5tCcLWtI5OMGDXJ2EEuEm0aijjQYo+8rR9aT7KrdEiWE4IH7OsD1a1N1RXpzGxJfpcE9cdPMueie9ZbSZAq/MIA5fSoB5npxOV2HUezZcc9s8CsT3/MnFPsXV6RurE+6K8PlK+KLSkC0RqTE5UVdr5EE89sEAUUuwLrF1GH7SyvKtg95ROYXqVwP6VgcY8XpsnaMQVg4nFdMWkT0SP5P6UiwWKMoTcs4E9gIGb1RFUVPJUN/i6K60W/cNNBWxo1M1WcAwqe3YO1iZn6jd0kTHTqhDr7f9RsIWnyT3lVpimHVXR62R34QdwlaX2NgQBXAp0qNk7ehrGMij1vNB9j+7JGxpw4EI1uRPEkKnEoZ4BHtUuSGWS0=\"\n" +
                "}";
        Type stringType = new TypeToken<BaseRequest>() {
        }.getType();
        Gson gson = new Gson();
        BaseRequest requestBody = gson.fromJson(request, stringType);
//        request = EncryptUtils.RSADecrypt(request, PublicKeyString.bitrustKey);
//        log.info("Request: " + request);
//        Type stringType = new TypeToken<EncryptionTestDto>() {
//        }.getType();
//        Gson gson = new Gson();
//        EncryptionTestDto requestBody = gson.fromJson(request, stringType);
//        log.info(requestBody);
    }
}
