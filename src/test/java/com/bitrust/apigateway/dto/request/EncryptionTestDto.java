package com.bitrust.apigateway.dto.request;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EncryptionTestDto {
    String description;
    String promotionCode;
    Boolean applyOnIsdn;
    String promotionName;
}
